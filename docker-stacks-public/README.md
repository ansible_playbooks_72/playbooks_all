

**Deploy pxc cluster**

```bash
ansible-playbook /etc/ansible/playbooks/docker-stacks-public/run.yaml --extra-vars "host=presta-manager-2" --tags="swarm_pxc_5_7_deploy"
```




**Deploy borg server **

```bash
ansible-playbook /etc/ansible/playbooks/docker-stacks-public/run.yaml --extra-vars "host=backuper.linux2be.com" --tags="borg_server_deploy"
```


**Deploy pxc8 cluster**

```sh
ansible_run_linux2be_com ansible-playbook /etc/ansible/playbooks/docker-stacks-public/run.yaml --extra-vars "host=human-asus-tuf" --tags="swarm_pxc_8_0_deploy"

```