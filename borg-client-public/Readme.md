


ansible-playbook /etc/ansible/playbooks/borg-client-public/run.yaml --tags="borg_client_backup_run_files" --extra-vars "host=vm-1  docker_image_borg_client=devsadds/borg-client:1.1.13-alpine-v1 \
borg_client_folder=/borg_client_v2 \
borg_repo_name={{ inventory_hostname }} \
borg_exclude_default=\"*.log,sh:**/temp,sh:**/pg11,sh:**prometheus/data,*temp*,sh:**gitlab/data/gitlab,sh:**data/postgresql/data,sh:**gitlab/data/gitlab,sh:**shop/vendor\" \
borg_server_host=backuper.linux2be.com \
backup_exclude=borg_client/borg_exclude.list \
borg_server_home_folder=/borg_server_v2 \
borg_server_container_name=borg_server_v2_borg-server_1 \
borg_server_backup_folder={{ borg_server_home_folder }}/data \
borg_remote_repo_url=ssh://borg@{{ borg_server_host }}:42456 \
borg_action=create \
backup_user=root \
borg_prune=0 \
borg_arhive=data \
borg_prune_options='--keep-hourly=1 --keep-daily=7 --keep-weekly=4 --keep-monthly=12 --prefix={{ borg_arhive }}-' serial=3"