

**exporters_deploy**

```bash
ansible-playbook /etc/ansible/playbooks/prometheus-monitoring-manage-public/run.yaml --extra-vars "host=localhost exporter_scrape_allow_hosts=0.0.0.0/0" --tags="prometheus_exporters_sart"
ansible-playbook /etc/ansible/playbooks/prometheus-monitoring-manage-public/run.yaml --extra-vars "host=localhost exporter_scrape_allow_hosts=0.0.0.0/0" --tags="prometheus_exporters_stop"
```

